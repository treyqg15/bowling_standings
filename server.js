//Lets require/import the HTTP module
var http = require('http');
var dispatcher = require('httpdispatcher');
var jsonfile = require('jsonfile');
var fs = require('fs');
var pdf = require('html-pdf');

//Lets define a port we want to listen to
const PORT=8080; 

//Lets use our dispatcher
function handleRequest(request, response){
    try {
        //log the request on console
        console.log(request.url);
        
        var allowedOrigins = ['http://127.0.0.1:8020', 'http://localhost:7700', 'http://127.0.0.1:9000', 'http://localhost:9000'];
		var origin = request.headers.origin;
		if(allowedOrigins.indexOf(origin) > -1){
		   response.setHeader('Access-Control-Allow-Origin', origin);
		}
		//response.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
		response.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
		response.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
		response.setHeader('Access-Control-Allow-Credentials', true);


        //Disptach
        dispatcher.dispatch(request, response);
    } catch(err) {
        console.log(err);
    }
}

dispatcher.onGet("/",function(req,res) {

    fs.readFile('./index.html', function (err, html) {
        if (err) {
            throw err;
        } else {
            console.log(html);
            res.write(html);
            res.end();
        } 
    });
});

//A sample GET request    
dispatcher.onGet("/page1", function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Page One');
});

//A sample POST request
dispatcher.onPost("/post1", function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Got Post Data');
});

dispatcher.onPost("/write", function(req, res) {

    res.writeHead(200, {'Content-Type': 'text/plain'});
    console.log('output file: ' + req.params.file);

    console.log(JSON.parse(req.params.doubles));
    
    //console.log(doubles);
    var file = './tmp/doubles2.json';
    jsonfile.writeFile(req.params.file, JSON.parse(req.params.doubles),function(err) {

        res.end(err);
    });

});

dispatcher.onPost("/print", function(req, res) {

    res.writeHead(200, {'Content-Type': 'text/plain'});
    console.log('output file: ' + req.params.file);

    var html = req.params.html;
    var file = req.params.file;
    var pdfFilePath = req.params.pdf;
    var options = { 
        format: 'Letter',
        orientation: 'landscape' 
    };

    //console.log(html);
    
    fs.writeFile(file, html,function(err) {

        res.end(err);
    });

    
    pdf.create(html, options).toFile(pdfFilePath, function(err, res) {
        if (err) return console.log(err);
            console.log(res); // { filename: '/app/businesscard.pdf' }
    });
    

});

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});