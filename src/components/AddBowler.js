import React from 'react';
import Styles from '../styles/styles'

export default class AddBowler extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			errors: {
				input: {
					name: true,
					lane_num: false,
					game_1: true,
					game_2: true,
					game_3: true,
					series: true,
					squad: true
				}
			},
			input: {
				name: '',
				lane_num: '',
				game_1: '',
				game_2: '',
				game_3: '',
				series: '',
				squad: ''
			}
		};
	}
	static defaultProps = {
		series: 0
	}
	static propTypes = {
		bowlerID: React.PropTypes.number.isRequired,
	}
	getStyles() {
		return {
			input: {
				error: {
					border: '1px solid red'
				},
				valid: {
					border: '1px solid #ccc'
				}
			}
		}
	}
	handleOnBlur(e) {
		if(e.target.value === '') {
			
			this.state.errors.input[e.target.name] = true;
		}
		else {
			this.state.errors.input[e.target.name] = false;
		}

		if(this.state.input.game_1 !== '' 
			&& this.state.input.game_2 !== '' 
			&& this.state.input.game_3 !== '' 
			&& this.state.input.series === '') {
			
			this.state.input.series = this.state.input.game_1 + this.state.input.game_2 + this.state.input.game_3;
		}

		this.setState(this.state);
	}
	handleOnChange(e) {
		var val = (e.target.type === 'number') ? parseInt(e.target.value) : e.target.value;
		this.state.input[e.target.name] = val;
		
		this.setState(this.state);
	}
	render() {


		return (
			<div>
				<input onChange={this.handleOnChange.bind(this)} onBlur={this.handleOnBlur.bind(this)} style={(this.state.errors.input.name) ? this.getStyles().input.error : this.getStyles().input.valid} data-isValid={this.state.errors.input.name} name="name" data-bowlerID={this.props.bowlerID} type="text" placeholder="Name" value={this.state.input.name} />
				<input onChange={this.handleOnChange.bind(this)} style={(this.state.errors.input.lane_num) ? this.getStyles().input.error : this.getStyles().input.valid} data-isValid={this.state.errors.input.lane_num} name="lane_num" data-bowlerID={this.props.bowlerID} type="number" placeholder="Lane #" value={this.state.input.lane_num} />
				<input onChange={this.handleOnChange.bind(this)} onBlur={this.handleOnBlur.bind(this)} style={(this.state.errors.input.squad) ? this.getStyles().input.error : this.getStyles().input.valid} data-isValid={this.state.errors.input.suad} name="squad" data-bowlerID={this.props.bowlerID} type="number" placeholder="Squad" value={this.state.input.suad} />
				<input onChange={this.handleOnChange.bind(this)} onBlur={this.handleOnBlur.bind(this)} style={(this.state.errors.input.game_1) ? this.getStyles().input.error : this.getStyles().input.valid} data-isValid={this.state.errors.input.game_1} name="game_1" data-bowlerID={this.props.bowlerID} type="number" placeholder="Game 1" value={this.state.input.game_1} />
				<input onChange={this.handleOnChange.bind(this)} onBlur={this.handleOnBlur.bind(this)} style={(this.state.errors.input.game_2) ? this.getStyles().input.error : this.getStyles().input.valid} data-isValid={this.state.errors.input.game_2} name="game_2" data-bowlerID={this.props.bowlerID} type="number" placeholder="Game 2" value={this.state.input.game_2} />
				<input onChange={this.handleOnChange.bind(this)} onBlur={this.handleOnBlur.bind(this)} style={(this.state.errors.input.game_3) ? this.getStyles().input.error : this.getStyles().input.valid} data-isValid={this.state.errors.input.game_3} name="game_3" data-bowlerID={this.props.bowlerID} type="number" placeholder="Game 3" value={this.state.input.game_3} />
				<input onChange={this.handleOnChange.bind(this)} onBlur={this.handleOnBlur.bind(this)} style={(this.state.errors.input.series) ? this.getStyles().input.error : this.getStyles().input.valid} data-isValid={this.state.errors.input.series} name="series" data-bowlerID={this.props.bowlerID} type="number" placeholder="Series" value={this.state.input.series} />

			</div>
		)
	}
}