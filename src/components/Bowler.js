import React from 'react';
import Styles from '../styles/styles'

export default class Bowler extends React.Component {
	static defaultProps = {
		pos: null,
		name: null,
		squad: null,
		lane_num: null,
		game1: null,
		game2: null,
		game3: null,
		series: null,
		total: null
	}
	static propTypes = {
		bowlerID: React.PropTypes.number.isRequired,
	}
	getStyles() {
		return {

		}
	}
	render() {

		return (
			<div>
				<input name="name" data-bowlerID={this.props.bowlerID} type="text" placeholder="Name" />
				<input name="lane_num" data-bowlerID={this.props.bowlerID} type="number" placeholder="Lane #" />
				<input name="game_1" data-bowlerID={this.props.bowlerID} type="number" placeholder="Game 1" />
				<input name="game_2" data-bowlerID={this.props.bowlerID} type="number" placeholder="Game 2" />
				<input name="game_3" data-bowlerID={this.props.bowlerID} type="number" placeholder="Game 3" />
				<input name="series" data-bowlerID={this.props.bowlerID} type="number" placeholder="Series" />

			</div>
		)
	}
}