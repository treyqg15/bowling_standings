import {List} from 'immutable';
import doublesJSON from '../doubles.json';
import mongoose from 'mongoose';
import http from 'http';
import request from 'request';


function compare(a,b) {
	var totalA = a[0];
	var totalB = b[0];

	if (totalA.total > totalB.total)
		return -1;
	if (totalA.total < totalB.total)
	    return 1;
	
	return 0;
}

export function setDoubles(state,doubles) {

	state = {
		doubles: JSON.parse(JSON.stringify(doubles)).sort(compare)
	};

	return state;
}

export function addDoubles(state,doubles) {

	state = {
		doubles: state.doubles.concat(doubles).sort(compare)
	};

	debugger;


	
	return state;
}

export function write(state) {

	var file = './tmp/doubles2.json';

 	request.post(
	    'http://localhost:8080/write',
	    { form: {doubles: JSON.stringify(state.doubles),file: file}},
	    function (error, response, body) {
		
			if (!error && response.statusCode == 200) {
	            console.log(body);
	        }
	    }
	);

	return state;
}

export function print(state,html) {


	debugger;

	var file = './tmp/standings.html';
	var pdf = './tmp/standings.pdf';
	request.post(
	    'http://localhost:8080/print',
	    { form: {html: html,file: file,pdf: pdf}},
	    function (error, response, body) {
		
			if (!error && response.statusCode == 200) {
	            console.log(body);
	        }
	    }
	);

	return state;

}