import React from 'react';
import ReactDOM from 'react-dom';
import Styles from './styles/styles';

import StandingsContainer from './views/StandingsContainer';
import doubles from '../tmp/doubles2.json'

class HelloMessage extends React.Component {
	getStyles() {
		return {
			container: {
				marginLeft: '14%'
			}
		}
	}
	render() {
    	return (
    		<div style={this.getStyles().container}>

    			<StandingsContainer doubles={doubles} />
    		</div>
    	)
  	}
}

ReactDOM.render(<HelloMessage />, document.body);