import {setDoubles,addDoubles,write,print} from './core'

export default function reducer(state, action) {
  switch(action.type) {
  	case 'SET_DOUBLES':
  		return setDoubles(state, action.doubles);
  	case 'ADD_DOUBLES':
  		return addDoubles(state, action.doubles);
  	case 'WRITE':
  		return write(state);
  	case 'PRINT':
  		return print(state, action.html);
  }
}