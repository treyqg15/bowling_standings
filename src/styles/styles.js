var Styles = {
	
	display: {
		inline: {
			display: 'inline'
		},
		inlineBlock: {
			display: 'inline-block'
		},
		block: {
			display: 'block'
		},
		none: {
			display: 'none'
		}
	},
	verticalAlign: {
		top: {
			verticalAlign: 'top'
		},
		bottom: {
			verticalAlign: 'bottom'
		}
	},
	border: {
		black: {
			all: {
				border: '1px solid black'
			},
			bottom: {
				borderBottom: '1px solid black'
			},
			left: {
				borderLeft: '1px solid black'
			},
			right: {
				borderRight: '1px solid black'
			}
		}
	},
	position: {
		relative: {
			position: 'relative'
		},
		absolute: {
			position: 'absolute'
		}
	},
	float: {
		left: {
			float: 'left'
		},
		right: {
			float: 'right'
		}
	},
	textAlign: {
		right: {
			textAlign: 'right'
		},
		center: {
			textAlign: 'center'
		}
	},
	width: {
		full: {
			width: '100%'
		}
	},
	margin: {
		auto: {
			left: {
				marginLeft: 'auto'
			},
			right: {
				marginRight: 'auto'
			}
		}
	}
};

module.exports = Styles;