import React from 'react';
import AddBowler from '../components/AddBowler'

export default class AddBowlerContainer extends React.Component {

	static propTypes = {
		bowlerID: React.PropTypes.number.isRequired,
		onSubmit: React.PropTypes.func.isRequired
	}
	render() {
		return (
			<div>
				<form onSubmit={this.props.onSubmit}>
					<AddBowler bowlerID={this.props.bowlerID + 1}/>
					<AddBowler bowlerID={this.props.bowlerID + 2}/>
					<input type="submit" />
				</form>
			</div>
		)
	}
}