import React from 'react';

import Styles from '../styles/styles';

export default class DoublesContainer extends React.Component {
	static defaultProps = {
		doubles: []
	}
	getStyles() {
		return {
			root: {
				border: '1px solid black',
				width: 652
			},
			pos: {
				container: {
					width: 50,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				value: {
					fontSize: 23,
					height: 40,
					verticalAlign: 'middle',
					background: '#ccc'
				}
			},
			name: {
				container: {
					width: 200,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				valueContainer: {
					background: '#ccc',
					height: 40
				}
			},
			squad: {
				container: {
					width: 100,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				value: {
					fontSize: 23,
					height: 40,
					verticalAlign: 'middle',
					background: '#ccc'
				}
			},
			lane_num: {
				container: {
					width: 80,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				valueContainer: {
					background: '#ccc',
					height: 40
				}
			},
			game_1: {
				container: {
					width: 80,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				valueContainer: {
					background: '#ccc',
					height: 40
				}
			},
			game_2: {
				container: {
					width: 80,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				valueContainer: {
					background: '#ccc',
					height: 40
				}
			},
			game_3: {
				container: {
					width: 80,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				valueContainer: {
					background: '#ccc',
					height: 40
				}
			},
			series: {
				container: {
					width: 80,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				valueContainer: {
					background: '#ccc',
					height: 40
				}
			},
			total: {
				container: {
					width: 80,
					display: 'inline-block',
					verticalAlign: 'top'
				},
				value: {
					fontSize: 23,
					height: 40,
					background: '#ccc'
				}
			}

		}
	}
	render() {

		var divs = {

		};

		var el = this.props.doubles.map(function(doubles,i) {

			divs = {
				pos: [],
				name: [],
				squad: [],
				lane_num: [],
				game_1: [],
				game_2: [],
				game_3: [],
				series: [],
				total: []
			};

			for (var prop in divs) {

				var style = null;
				var val = null;
				var background = {};

				if(i % 2)
					background = {background: 'white'};

				switch(prop) {
					case 'pos':
						style = Object.assign({},this.getStyles().pos.value,background);
						val = i + 1;
						break;
					case 'squad':
						style = Object.assign({},this.getStyles().squad.value,background);
						val = doubles[0][prop];
						break;
					case 'total':
						style = Object.assign({},this.getStyles().total.value,background);
						val = doubles[0][prop];
						break;
					default:

						style = Object.assign({},this.getStyles()[prop].valueContainer,background);
						val = <div><div style={this.getStyles()[prop].value}>{doubles[0][prop]}</div><div style={this.getStyles()[prop].value}>{doubles[1][prop]}</div></div>;
						break;
				}

				divs[prop].push(<div style={style} key={i}>{val}</div>);
			}
			
			return divs;
		}.bind(this));

		var red = el.reduce(function(prev, curr) {

			return {
				pos: prev.pos.concat(curr.pos),
				name: prev.name.concat(curr.name),
				squad: prev.squad.concat(curr.squad),
				lane_num: prev.lane_num.concat(curr.lane_num),
				game_1: prev.game_1.concat(curr.game_1),
				game_2: prev.game_2.concat(curr.game_2),
				game_3: prev.game_3.concat(curr.game_3),
				series: prev.series.concat(curr.series),
				total: prev.total.concat(curr.total),
			};
		});

		return (
			<div>
				<div style={this.getStyles().root}>
					<div style={this.getStyles().pos.container}>
						<div style={this.getStyles().pos.title}>
							POS
						</div>
						<div>
							{red.pos}
						</div>
					</div>

					<div style={this.getStyles().name.container}>
						<div>
							NAMES
						</div>
						<div style={this.getStyles().name.value}>
							{red.name}
						</div>
					</div>

					<div style={this.getStyles().game_1.container}>
						<div>
							GAME 1
						</div>
						<div>
							{red.game_1}
						</div>
					</div>

					<div style={this.getStyles().game_2.container}>
						<div>
							GAME 2
						</div>
						<div>
							{red.game_2}
						</div>
					</div>

					<div style={this.getStyles().game_3.container}>
						<div>
							GAME 3
						</div>
						<div>
							{red.game_3}
						</div>
					</div>

					<div style={this.getStyles().series.container}>
						<div>
							SERIES
						</div>
						<div>
							{red.series}
						</div>
					</div>

					<div style={this.getStyles().total.container}>
						<div style={this.getStyles().total.title}>
							TOTAL
						</div>
						<div>
							{red.total}
						</div>
					</div>
					
				</div>
			</div>
		)
	}
}