import React from 'react';
import ReactDOM from 'react-dom';
import AddBowlerContainer from './AddBowlerContainer';
import DoublesContainer from './DoublesContainer';
import Styles from '../styles/styles';

import reducer from '../reducer';

export default class StandingsContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			doubles: props.doubles
		};
	}
	static defaultProps = {
		doubles: []
	}
	static propTypes = {
		doubles: React.PropTypes.array
	}
	getStyles() {
		return {
			headerTitle: {
				display: 'inline-block'
			},
			posColumn: {
				display: 'inline-block',
				width: 50,
				textAlign: 'center'
			},
			namesColumn: {
				display: 'inline-block',
				width: 100
			},
			squadColumn: {
				display: 'inline-block',
				width: 100
			},
			laneNumColumn: {
				display: 'inline-block',
				width: 80
			},
			game1Column: {
				display: 'inline-block',
				width: 80
			},
			game2Column: {
				display: 'inline-block',
				width: 80
			},
			game3Column: {
				display: 'inline-block',
				width: 80
			},
			seriesColumn: {
				display: 'inline-block',
				width: 80
			},
			totalColumn: {
				display: 'inline-block',
				width: 40
			}

		}
	}
	handleOnSubmit(e) {
		e.preventDefault();

		var bowlers = {};

		for(var i in e.target) {

			if(isNaN(i))
				continue;

			var bowlerID = e.target[i].dataset.bowlerid;
			var isValid = e.target[i].dataset.isvalid;
			if(isValid === "true")
				return;

			var name = e.target[i].name;
			var value = e.target[i].value;

			if(typeof bowlers[bowlerID] === 'undefined')
				bowlers[bowlerID] = {};


			bowlers[bowlerID][name] = value;
		}

		var addDoubles = [];
		for(var i in bowlers) {
			if(i === 'undefined')
				continue;

			addDoubles.push(bowlers[i]);
		}

		//figure total
		var total = parseInt(addDoubles[0].series) + parseInt(addDoubles[1].series);
		addDoubles[0].total = total;
		addDoubles[1].total = total;

		var action = {type: 'ADD_DOUBLES', doubles: [addDoubles]};
		var nextState = reducer(this.state, action);

		action = {type: 'WRITE'};
		nextState = reducer(nextState,action);

		this.setState(nextState);


	}

	print() {

		var el = ReactDOM.findDOMNode(this.refs.refDoubles);
		var action = {type: 'PRINT', html: el.innerHTML};
		var nextState = reducer(this.state, action);
	}

	render() {

		var arr = [];

	    return (

	    	<div>
	    		<AddBowlerContainer 
	    			bowlerID={this.props.doubles.length} 
	    			onSubmit={this.handleOnSubmit.bind(this)}
	    		/>
	    		<input onClick={this.print.bind(this)} type="button" value="PRINT" />
	    		<DoublesContainer ref='refDoubles'
	    			doubles={this.state.doubles}
	    		/>
	    	</div>
	    )
  	}
}