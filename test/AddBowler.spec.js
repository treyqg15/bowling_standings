import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import AddBowler from '../src/components/AddBowler';

describe('<AddBowlerContainer/>', function () {

	it('calculates series input', function() {
  		const wrapper = mount(<AddBowler/>);
  		var game1 = wrapper.find('input[name="game_1"]');
  		var game2 = wrapper.find('input[name="game_2"]');
  		var game3 = wrapper.find('input[name="game_3"]');

  		game1.node.value = 2;
  		game1.simulate('change');
  		game1.simulate('blur');
  		
  		game2.node.value = 2;
  		game2.node.value = 2;
  		game2.simulate('change');
  		game2.simulate('blur');

  		game3.node.value = 3;
  		game3.node.value = 2;
  		game3.simulate('change');
  		game3.simulate('blur');

      expect(wrapper.state().input.series).to.equal(6);
  	}); 

});