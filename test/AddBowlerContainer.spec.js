import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import StandingsContainer from '../src/views/StandingsContainer';
import AddBowlerContainer from '../src/views/AddBowlerContainer';
import AddBowler from '../src/components/AddBowler';


describe('<AddBowlerContainer/>', function () {

	it('renders proper # of AddBowler Components', function() {
  		const wrapper = mount(<AddBowlerContainer/>);
  		expect(wrapper.find(AddBowler)).to.have.length.of(2);
  	});

  	it('create doubles entry', function() {
  		const wrapper = mount(<StandingsContainer/>);
  		const addBowlerContainerWrapper = wrapper.find(AddBowlerContainer);

  		var bowler1 = addBowlerContainerWrapper.find(AddBowler).at(0);
  		var bowler2 = addBowlerContainerWrapper.find(AddBowler).at(1);

  		var name = bowler1.find('input[name="name"]');
  		var lane_num = bowler1.find('input[name="lane_num"]');
  		var game1 = bowler1.find('input[name="game_1"]');
  		var game2 = bowler1.find('input[name="game_2"]');
  		var game3 = bowler1.find('input[name="game_3"]');

  		name.node.value = 'TestBowler1';
  		lane_num.node.value = 1;
  		game1.node.value = 2;
  		game2.node.value = 2;
  		game3.node.value = 2;

  		name = bowler2.find('input[name="name"]');
  		lane_num = bowler2.find('input[name="lane_num"]');
  		game1 = bowler2.find('input[name="game_1"]');
  		game2 = bowler2.find('input[name="game_2"]');
  		game3 = bowler2.find('input[name="game_3"]');

  		name.node.value = 'TestBowler2';
  		lane_num.node.value = 1;
  		game1.node.value = 2;
  		game2.node.value = 2;
  		game3.node.value = 2;

  		//console.log(addBowlerContainerWrapper.find('input[type="submit"]').get(0).click());
  		addBowlerContainerWrapper.find('input[type="submit"]').get(0).click();
  	});

});