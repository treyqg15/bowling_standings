import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import {List,Map} from 'immutable';

import StandingsContainer from '../src/views/StandingsContainer';

import doubles from '../tmp/doubles.json'
import reducer from '../src/reducer';
import DoublesContainer from '../src/views/DoublesContainer';
import AddBowlerContainer from '../src/views/AddBowlerContainer';

describe('<StandingsContainer/>', function () {
	

  	it('checking prop types', function() {
  		const wrapper = mount(<StandingsContainer doubles={doubles}/>);
  		expect(wrapper.props().doubles).to.be.defined;
  		expect(wrapper.props().doubles).to.be.instanceof(Array);
  	});

  	it('renders AddBowlerContainer Component', function() {
  		const wrapper = shallow(<StandingsContainer/>);
  		expect(wrapper.find(AddBowlerContainer)).to.exist;
  	});
});