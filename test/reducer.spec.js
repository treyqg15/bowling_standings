import React from 'react';
import {expect} from 'chai';
import request from 'request';
import jsonfile from 'jsonfile';

import doubles from '../doubles.json'
import reducer from '../src/reducer.js';

describe('reducer', function() {

	it('set doubles', function() {

  		const initialState = Object.freeze({});
  		const action = {type: 'SET_DOUBLES', doubles: doubles};
  		const nextState = reducer(initialState, action);
  		
  		expect(nextState).to.deep.equal({doubles:doubles});
  		expect(initialState).to.deep.equal({});
  	});

  	it('add doubles', function() {

  		const initialState = Object.freeze({doubles: doubles});
  		const addDoubles = [{"name":"sdfsf","lane_num":"","game_1":"","game_2":"","game_3":"","series":""},{"name":"343ref","lane_num":"","game_1":"","game_2":"","game_3":"","series":""}];
  		const newDoubles = doubles.concat([addDoubles]);
  		
  		const action = {type: 'ADD_DOUBLES', doubles: [addDoubles]};
  		const nextState = reducer(initialState, action);

  		expect(nextState).to.deep.equal({doubles:newDoubles});
  		expect(initialState).to.deep.equal({doubles:doubles});
  	});

  	it('write', function(done) {

  		var file = './tmp/doubles.spec.json';

  		request.post(
		    'http://localhost:8080/write',
		    { form: {doubles: JSON.stringify(doubles),file: file}},
		    function (error, response, body) {
			
				if (!error && response.statusCode == 200) {

		        }
		    	
		    	expect(error).to.be.null;
		    	
	    		jsonfile.readFile(file, function(err,obj) {
	    			expect(error).to.be.null;
	    			expect(obj).to.deep.equal(doubles);

	    			done();
	    		});;
		    }
		);

  	});
});