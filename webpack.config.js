const path = require('path');
const webpack = require('webpack');

// env
const buildDirectory = './dist/';

module.exports = {
  entry: './src/index.js',
  devServer: {
    hot: true,
    inline: true,
    port: 7700,
    historyApiFallback: true,
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
  },
  output: {
    path: path.resolve(buildDirectory),
    filename: 'build.js',
    publicPath: 'http://localhost:7700/dist',
  },
  node: {
    fs: "empty",
    net: "empty",
    dns: "empty",
    tls: "empty",
    module: "empty"
  },
  externals: {
    'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'babel',
      query: {
        presets: ['react', 'es2016', 'stage-0'],
      },
    },
    {
      test: /\.json$/, 
      loader: 'json-loader' 
    }]
  },
  plugins: [],
};